import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import CardMenu from './components/cardmenu/CardMenu';
import ItemsMunu from './components/itemslist/ItemsList';
import ThemeContextProvider from './providers/ThemeProvider';
import store from './store/store';
import { apolloClient } from './constants/StringConstants';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import { ApolloProvider } from 'react-apollo';
import { Provider } from 'react-redux';

ReactDOM.render(
  <Router>
      <ApolloProvider client={apolloClient}>
          <Provider store={store}>
            <ThemeContextProvider>
              <Route exact path='/carditem' component={ItemsMunu} />
              <Route exact path='/' component={CardMenu} />
            </ThemeContextProvider>
          </Provider>
      </ApolloProvider>
  </Router>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
