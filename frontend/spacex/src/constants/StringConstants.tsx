import ApolloClient from 'apollo-boost';

export const apolloClient = new ApolloClient({ uri: 'http://api.spacex.land/graphql/' })
