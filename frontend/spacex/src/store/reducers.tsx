import { TypesPopupJson } from '../components/itemspopup/Popup'

type DefaultState = {
  popupJson: TypesPopupJson | {},
  statusPopup: boolean,
  colTheme: string
}

const defaultState: DefaultState = {
  popupJson: {},
  statusPopup: false,
  colTheme: 'light'
}

function MainReducer(state=defaultState, action) {
  switch(action.type) {
    case 'POPUP':
      return {...state, popupJson: action.value}
    case 'STATUSPOPUP':
      return {...state, statusPopup: action.value}
    case 'COLORTHEME':
      return {...state, colTheme: action.value}
    default:
      return state;
  }
}

export default MainReducer
