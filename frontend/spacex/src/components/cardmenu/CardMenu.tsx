import React, { useContext } from 'react';
import './CardMenu.css';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import { ThemeContext } from '../../providers/ThemeProvider';
import store from '../../store/store';
import { ThemeProvider } from '@material-ui/core/styles';

type CardMenu = {
  history: any
}

const RedirectTo = (props: {history:any}, loc:string) => {
  switch(loc) {
    case 'Ships':
      return props.history.push({
        pathname: '/carditem',
        search: '?card=ships',
        state: {name: 'Ships'}
      })
    case 'Rockets':
      return props.history.push({
        pathname: '/carditem',
        search: '?card=rockets',
        state: {name: 'Rockets'}
      })
    case 'Launch Pads':
      return props.history.push({
        pathname: '/carditem',
        search: '?card=launchpads',
        state: {name: 'Launch Pads'}
      })
    case 'History':
      return props.history.push({
        pathname: '/carditem',
        search: '?card=history',
        state: {name: 'History'}
      })
  }
}

const CardMenu = (props:CardMenu) => {
  const themeValue = useContext(ThemeContext)

  return (
    <ThemeProvider theme={themeValue}>
      <div className='card-wrapper'>
        <div className='dark-mode-select'>
          <FormControlLabel
            control={<Switch color="primary" />}
            label="Dark Mode: "
            checked={themeValue.palette.type == 'dark' ? true : false}
            labelPlacement="start"
            onChange={() => store.dispatch({type: 'COLORTHEME', value: themeValue.palette.type == 'dark'? 'light' : 'dark' })}
          />
        </div>
        <Card className='card-size'>
          <CardContent>
            <Typography>Ships</Typography>
            <img className='menu-card-image' src='https://i.imgur.com/MtEgYbY.jpg' />
          </CardContent>
          <CardActions>
            <Button
              size="small"
              className="card-buttom"
              onClick={() => RedirectTo(props, 'Ships')}
            >Learn More</Button>
          </CardActions>
        </Card>
        <Card className='card-size'>
          <CardContent>
            <Typography>Rockets</Typography>
            <img className='menu-card-image' src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTGwfopWq1nOuYRLnDxp-Jbks-bHiWuj94nFxlK4eiaF29y_98U&s' />
          </CardContent>
          <CardActions>
            <Button
              size="small"
              className="card-buttom"
              onClick={() => RedirectTo(props, 'Rockets')}
            >Learn More</Button>
          </CardActions>
        </Card>
        <Card className='card-size'>
          <CardContent>
            <Typography>Launch Pads</Typography>
            <img className='menu-card-image' src='https://cdn.pastemagazine.com/www/articles/main%20image%20%288%29.jpg' />
          </CardContent>
          <CardActions>
            <Button
              size="small"
              className="card-buttom"
              onClick={() => RedirectTo(props, 'Launch Pads')}
            >Learn More</Button>
          </CardActions>
        </Card>
        <Card className='card-size'>
          <CardContent>
            <Typography>History</Typography>
            <img className='menu-card-image' src='https://images-na.ssl-images-amazon.com/images/I/51WE6HS6AQL._SX258_BO1,204,203,200_.jpg' />
          </CardContent>
          <CardActions>
            <Button
              size="small"
              className="card-buttom"
              onClick={() => RedirectTo(props, 'History')}
            >Learn More</Button>
          </CardActions>
        </Card>
      </div>
    </ThemeProvider>
  )
}

export default CardMenu;
