import { gql } from 'apollo-boost';

export const getRocketInfo = gql`
{
  launchesPast(limit: 10) {
    launch_date_local
    launch_site {
      site_name_long
    }
    links {
      video_link
      flickr_images
    }
    rocket {
      rocket_name
      second_stage {
        payloads {
          payload_type
          payload_mass_kg
          payload_mass_lbs
        }
      }
      rocket {
        id
      }
    }
  }
}
`

export const getlaunchpadsInfo = gql`
{
  launchpads {
    name
    location {
      latitude
      longitude
      name
      region
    }
  }
}
`
export const getShipsInfo = gql`
{
  launchesPast(limit: 10) {
    ships {
      name
      home_port
      image
    }
  }
}
`
export const getHistoryInfo = gql`
{
  histories {
    id
    event_date_utc
    title
    details
  }
}
`