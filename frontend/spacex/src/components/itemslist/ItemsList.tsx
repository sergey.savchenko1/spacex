import React, { useState, useEffect, useContext } from 'react';
import '../cardmenu/CardMenu.css';
import './ItemsList.css';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import TopBarProgress from 'react-topbar-progress-indicator';
import InfoPopup from '../itemspopup/Popup';
import store from '../../store/store';
import { ThemeContext } from '../../providers/ThemeProvider';
import { BackIcon }  from '../../constants/MediaConstant';
import { useQuery } from '@apollo/react-hooks';
import { getRocketInfo, getlaunchpadsInfo, getShipsInfo, getHistoryInfo } from './graphqlquery';
import { ThemeProvider } from '@material-ui/core/styles';

type ItemsMunu = {
  history: any,
  location: {search:string, state: {name: string}}
}

type JsonlaunchesPast = {
  rocket: {rocket_name: string},
  links: {flickr_images: Array<string>}
}

type RocketView = {
  IsListView: boolean,
  launchesPast: Array<JsonlaunchesPast>,
  location: string
}

type JsonLaunchPad = {
  name: string
}

type LaunchPads = {
  IsListView: boolean,
  launchpads: Array<JsonLaunchPad>,
  location: string
}

type JsonShips = {
  name: string,
  image: string
}

type JsonLaunchesPast = {
  ships: Array<JsonShips>
}

type Ships = {
  IsListView: boolean,
  launchesPast: Array<JsonLaunchesPast>,
  location: string
}

type JsonHistories = {
  title: string,
  event_date_utc: string,
}

type History = {
  IsListView: boolean,
  histories: Array<JsonHistories>,
  location: string
}

type CardView = {
  isListView: boolean,
  posMenu: any,
  location: string,
}

const hundleClick = (obj: Object, location: string) => {
  obj['location'] = location
  store.dispatch({type: 'STATUSPOPUP', value: true})
  store.dispatch({type: 'POPUP', value: obj})
}

const RocketView = (props:RocketView) => {
  return (
    <div className='card-items-wrapper'>
      {props.launchesPast.map((obj, i) => {
        return (
          <Card className={props.IsListView ? 'card-size-items-list' : 'card-size-items'}
            key={i}
          >
            <CardContent>
              <Typography>{obj.rocket.rocket_name}</Typography>
              <img src={obj.links.flickr_images[0]} />
            </CardContent>
            <CardActions>
              <Button
                size="small"
                className="card-buttom card-buttom-item"
                onClick={() => hundleClick(obj, props.location)}
              >Detail</Button>
            </CardActions>
          </Card>
        )
      })}
    </div>
  )
}

const LaunchPads = (props:LaunchPads) => {
  return (
    <div className='card-items-wrapper'>
      {props.launchpads.map((obj, i) => {
        return (
          <Card className={props.IsListView ? 'card-size-items-list' : 'card-size-items'}
            key={i}
          >
            <CardContent>
              <Typography>{obj.name}</Typography>
            </CardContent>
            <CardActions>
              <Button
                size="small"
                className="card-buttom card-buttom-item"
                onClick={() => hundleClick(obj, props.location)}
              >Detail</Button>
            </CardActions>
          </Card>
        )
      })}
    </div>
  )
}

const Ships = (props:Ships) => {
  return (
    <div className='card-items-wrapper'>
      {props.launchesPast.map((obj) => {
          return obj.ships.map((shipObj, i) => {
            if (!shipObj) {
              return
            }
            return(
              <Card className={props.IsListView ? 'card-size-items-list' : 'card-size-items'}
                key={i}
              >
                <CardContent>
                  <Typography>{shipObj.name.substr(0, 20)}</Typography>
                  <img src={shipObj.image} />
                </CardContent>
                <CardActions>
                  <Button
                    size="small"
                    className="card-buttom card-buttom-item"
                    onClick={() => hundleClick(obj, props.location)}
                  >Detail</Button>
                </CardActions>
              </Card>
          )
        })
      })}
    </div>
  )
}

const History = (props: History) => {
  return (
    <div className='card-items-wrapper'>
      {props.histories.map((obj, i) => {
        return (
          <Card className={props.IsListView ? 'card-size-items-list' : 'card-size-items'}
            key={i}
          >
            <CardContent>
              <Typography>{obj.title}</Typography>
              <Typography>{obj.event_date_utc.substr(0, 10)}</Typography>
            </CardContent>
            <CardActions>
              <Button
                size="small"
                className="card-buttom card-buttom-item"
                onClick={() => hundleClick(obj, props.location)}
              >Detail</Button>
            </CardActions>
          </Card>
        )
      })}
    </div>
  )
}

const CardView = (props:CardView) => {
  const { loading, error, data } = useQuery(props.posMenu);
  if (loading) return (<TopBarProgress />)
  if (error) return <p>Error</p>

  switch(props.location) {
    case 'Rockets':
      return <RocketView IsListView={props.isListView} launchesPast={data.launchesPast} location={props.location} />
    case 'Launch Pads':
      return <LaunchPads IsListView={props.isListView} launchpads={data.launchpads} location={props.location} />
    case 'Ships':
      return <Ships IsListView={props.isListView} launchesPast={data.launchesPast} location={props.location} />
    default:
      return <History IsListView={props.isListView} histories={data.histories} location={props.location} />
  }
}

const ItemsMunu = (props:ItemsMunu) => {
  const themeValue = useContext(ThemeContext)
  const [header, setHeader] = useState('-')
  const [listView, setlistView] = useState(false)
  const query = new URLSearchParams(props.location.search)
  const searchParam = query.get('card')

  switch(searchParam) {
    case 'rockets':
      var queryVal = getRocketInfo
      break
    case 'launchpads':
      var queryVal = getlaunchpadsInfo
      break
    case 'ships':
      var queryVal = getShipsInfo
      break
    default:
      var queryVal = getHistoryInfo
  }

  return (
          <ThemeProvider theme={themeValue}>
            <div className='card-wrapper card-wrapper-items'>
              <svg dangerouslySetInnerHTML={{__html: BackIcon }} className='back-icon' onClick={() => props.history.push('/')} />
              <p className='wrapper'>{props.location.state.name}</p>
              <div>
                <FormControlLabel
                  control={<Switch color="primary" />}
                  label="List View: "
                  checked={listView}
                  labelPlacement="start"
                  onChange={() => setlistView(listView ? false : true)}
                />
                <FormControlLabel
                  control={<Switch color="primary" />}
                  label="Dark Mode: "
                  checked={themeValue.palette.type == 'dark' ? true : false}
                  labelPlacement="start"
                  onChange={() => store.dispatch({type: 'COLORTHEME', value: themeValue.palette.type == 'dark'? 'light' : 'dark' })}
                />
              </div>
              <CardView posMenu={queryVal} isListView={listView} location={props.location.state.name} />
              <InfoPopup />
            </div>
          </ThemeProvider>
  )
}

export default ItemsMunu;
