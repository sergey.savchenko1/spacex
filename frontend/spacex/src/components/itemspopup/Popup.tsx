import React, { useState, useEffect } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import store from '../../store/store';
import { connect } from 'react-redux';
import '../itemslist/ItemsList.css';

type PopupJsonLaunchPads = {
  name: string,
  location: string
}

type LaunchPadsPopup = {
  StatusPopup: boolean,
  PopupJson: PopupJsonLaunchPads
}

type JsonRcketsPopup = {
  rocket: {rocket_name: string},
  launch_site: {site_name_long: string},
  links: {flickr_images: Array<string>},
  location: string
}

type RocketsPopup = {
  StatusPopup: boolean,
  PopupJson: JsonRcketsPopup
}

type JsonShipsPopup = {
  ships: Array<{name: string, image: string}>,
  location: string
}

type ShipsPopup = {
  StatusPopup: boolean,
  PopupJson: JsonShipsPopup
}

type JsonHistoryPopup = {
  title: string,
  details: string,
  event_date_utc: string,
  location: string
}

type HistoryPopup = {
  StatusPopup: boolean,
  PopupJson: JsonHistoryPopup
}
export type TypesPopupJson = {
  PopupJson: PopupJsonLaunchPads & JsonRcketsPopup & JsonShipsPopup & JsonHistoryPopup,
  StatusPopup: boolean
}

const handleClose = () => {
  store.dispatch({type: 'STATUSPOPUP', value: false})
};

const LaunchPadsPopup = (props:LaunchPadsPopup) => {
  return (<Dialog open={props.StatusPopup} onClose={handleClose} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">{props.PopupJson.name}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              To subscribe to this website, please enter your email address here. We will send updates
              occasionally.
            </DialogContentText>
          </DialogContent>
        </Dialog>)
}

const RocketsPopup = (props:RocketsPopup) => {
  return (<Dialog open={props.StatusPopup} onClose={handleClose} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">{props.PopupJson.rocket.rocket_name}</DialogTitle>
            <DialogContent>
              <DialogContentText>
                {props.PopupJson.launch_site.site_name_long}
                {props.PopupJson.links.flickr_images.map((obj, i) => {
                  return <img className='image-popup' src={obj} key={i} />
                })}
              </DialogContentText>
            </DialogContent>
          </Dialog>)
}

const ShipsPopup = (props:ShipsPopup) => {
  return (<Dialog open={props.StatusPopup} onClose={handleClose} aria-labelledby="form-dialog-title">
            {props.PopupJson.ships.map((obj, i) => {
              if (!obj) {
                return
              }
              return <div key={i}>
                        <DialogTitle id="form-dialog-title">{obj.name}</DialogTitle>
                        <img className='image-popup' src={obj.image} />
                    </div>
            })}
          </Dialog>)
}

const HistoryPopup = (props:HistoryPopup) => {
  return (<Dialog open={props.StatusPopup} onClose={handleClose} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">{props.PopupJson.title}</DialogTitle>
            <DialogContent>
              <DialogContentText>
                {props.PopupJson.details}
              </DialogContentText>
              <DialogContentText>
                {props.PopupJson.event_date_utc.substr(0, 10)}
              </DialogContentText>
            </DialogContent>
          </Dialog>)
}

const InfoPopup = (props:TypesPopupJson) => {

  switch(props.PopupJson.location) {
    case 'Launch Pads':
      return <LaunchPadsPopup StatusPopup={props.StatusPopup} PopupJson={props.PopupJson} />
    case 'Rockets':
      return <RocketsPopup StatusPopup={props.StatusPopup} PopupJson={props.PopupJson} />
    case 'Ships':
      return <ShipsPopup StatusPopup={props.StatusPopup} PopupJson={props.PopupJson} />
    case 'History':
      return <HistoryPopup StatusPopup={props.StatusPopup} PopupJson={props.PopupJson} />
    default:
      return <div></div>
  }
}

const mapStateToProps = (state) => {
  return {
    PopupJson: state.popupJson,
    StatusPopup: state.statusPopup,
  };
}

export default connect(mapStateToProps)(InfoPopup)
