import React, { createContext, useReducer } from 'react';
import { createMuiTheme,  } from '@material-ui/core/styles';
import { connect } from 'react-redux';

const InitTheme = (props:string) => {
  return createMuiTheme({
    palette: {
      type: props == 'light' ? 'light' : 'dark'
    }
  });
}

export const ThemeContext = createContext(createMuiTheme({}))

const ThemeContextProvider = (props: {colTheme: string, children: Object}) => {
  return (
    <ThemeContext.Provider value={InitTheme(props.colTheme)}>
      {props.children}
    </ThemeContext.Provider>
  )
}

const mapStateToProps = (state: {colTheme: string}) => {
  return {
    colTheme: state.colTheme,
  };
}

export default connect(mapStateToProps)(ThemeContextProvider);
